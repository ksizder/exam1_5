import sys
from copy import copy, deepcopy
def makezerolist(n):
    listofzeros = [0] * n
    return listofzeros
def form_matrix(matrix, row, column):
    length = len(matrix[0])
    result = [[0 for x in range(length - 1)] for y in range(length - 1)]
    new_length = len(result[0])
    _k = 0
    _j = 0
    for k in range(length):
        for j in range(length):
            if(not((k == row) or (j == column))):
                result[_k][_j] = matrix[k][j]
                _j += 1
                if(_j == new_length):
                    _k += 1
                    _j = 0

    return result
def det(matrix):
    length = len(matrix[0])
    if(length == 2):
        return matrix[0][0] * matrix[1][1] - matrix[1][0] * matrix[0][1]
    else:
        p_det = 0.0
        sign = 1.0
        for k in range(length):
           p_det  = p_det +  matrix[0][k] * sign * det(form_matrix(matrix, 0, k))
           sign = sign * (-1.0)
        return p_det
def solve_kramer(matrix, vector):
    save = deepcopy(matrix)
    result = makezerolist(len(vector))
    length = len(matrix[0])
    maindet = det(matrix)
    print(sys.getsizeof(save) + sys.getsizeof(matrix) + sys.getsizeof(vector) + sys.getsizeof(result) + sys.getsizeof(length) + sys.getsizeof(maindet))
    if(not(maindet == 0)):
        for i in range(length):
            for k in range(length):
                matrix[k][i] = vector[k]
            result[i] = det(matrix) / maindet
            matrix = deepcopy(save)
    return result


