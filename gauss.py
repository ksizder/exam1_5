def makezerolist(n):
    listofzeros = [0] * n
    return listofzeros
def input_vector(n):
    lst = []
    for i in range(0, n):
        ele = float(input())
        lst.append(ele)  # adding the element
    return lst
def input_matrix(m_order):
    matrix = [[float(input('Enter the matrix element\n')) for x in range(m_order)] for y in range(m_order)]
    return matrix
def forward(matrix, order):
    for k in range(1, order):
        for j in range(k + 1, order):
            matrix[k][j] = 0
    #return matrix

def solve_gauss():
    order = int(input('Enter the matrix order:\n'))
    print('Enter the coef vector:\n')
    coef = input_vector(order)
    print('Enter the matrix:\n')
    matrix = input_matrix(order)
    result = makezerolist(order)
    for k in range(1, order):
        for j in range(k):
            d = matrix[k][j] / matrix[j][j]
            for a in range(order):
                matrix[k][a] = matrix[k][a] - d * matrix[j][a]
            coef[k] = coef[k] - d * coef[j]
    for k in reversed(range(order)):
        s = 0
        for n in range(order):
            temp = matrix[k][n] * result[n]
            s = s + temp
        result[k] = (coef[k] - s) / matrix[k][k]




    print(result)

