import gauss
import kramer
import time
import random
#gauss.solve_gauss()

#print(kramer.solve_kramer([[3.0, -2.0, 4.0], [3.0, 4.0, -2.0], [2.0, -1.0, -1.0]], [21, 9, 10]))
#tests below
for i in range(3, 11):

    Matrix = [[random.randint(99, 500) for x in range(i)] for y in range(i)]
    Vector = [random.randint(99, 500) for x in range(i)]
    #start_time = time.time()
    kramer.solve_kramer(Matrix, Vector)
    #print("--- %s seconds ---" % (time.time() - start_time))
